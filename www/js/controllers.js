angular.module('starter.controllers', ['ionic.native'])

.controller('StartCtrl', function($scope, $state, ApiService, shapeFactory) {
		$scope.shapes = [
			{ id: 'cube', name: 'Cube'},
			{ id: 'cone', name: 'Cone'},
			{ id: 'ring', name: 'Ring'},
			// { id: 'triangle', name: 'Triangle'},,
			// { id: 'box', name: 'Box'},
			// { id: 'tube', name: 'Tube'},
		];

		$scope.colors = [
			{ id: 'red', name: 'Red'},
			{ id: 'blue', name: 'Blue'},
			{ id: 'green', name: 'Green'},
			{ id: 'yellow', name: 'Yellow'}
		];

		$scope.data = {
			ipAddress: '192.168.0.13',
			shape: 'cube',
			color: 'red',
			location: {
				x: -5,
				y: 3,
				z: 10
			}
		};

		$scope.start = function() {
			ApiService.start($scope.data).then(function(res) {
				$state.go('home.dash');
			});
		};
})

.controller('ScenarioCtrl', function($scope, $http, $state, ApiService, shapeFactory) {
		// var url = ApiService.hostIp + 'pos';
		//
		// if(url === 'pos') {
		// 	$state.go('home.start');
		// }
		//
		// var scene = new THREE.Scene();
		// var camera = new THREE.PerspectiveCamera(75,window.innerWidth/window.innerHeight,1,10000);
		//
		// var renderer = new THREE.WebGLRenderer();
		// renderer.setSize(window.innerWidth,window.innerHeight);
		//
		// effect = new THREE.StereoEffect(renderer);
		// effect.eyeSeparation = 1;
		// effect.setSize( window.innerWidth, window.innerHeight );
		//
		// document.getElementById("canvasContainer").appendChild(renderer.domElement);
		//
		// var axisHelper = new THREE.AxisHelper( 10 );
		// scene.add( axisHelper );
		//
		// var shape = shapeFactory.getShape({
		// 	shape: ApiService.shape,
		// 	color: ApiService.color
		// });
		// scene.add(shape);
		//
		// shape.position.set(4,3,-10);
		// camera.position.set(5,5,10);
		//
		// var intervalID = setInterval(function() {
		// 			$http.get(url).then(function(res){
		// 			shape.rotation.x = res.data.x/360*2*Math.PI;
		// 			shape.rotation.y = res.data.y/180*2*Math.PI;
		// 			shape.rotation.z = -res.data.z/360*2*Math.PI;
		// 		});
		// 	}, 100);
		//
		// function render() {
		// 	requestAnimationFrame(render);
		// 	renderer.render(scene,camera);
		// //	effect.render(scene,camera);
		// }
		//
		// render();
})

.controller('DashCtrl', function($scope, $ionicPlatform, $http, $cordovaDeviceMotion, $cordovaDeviceOrientation, ApiService, shapeFactory) {
	var url = ApiService.hostIp + 'pos';

	var scene = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight, 1, 10000);

	var renderer = new THREE.WebGLRenderer();
	renderer.setSize(window.innerWidth, window.innerHeight);

	effect = new THREE.StereoEffect(renderer);
	effect.eyeSeparation = 1;
	effect.setSize( window.innerWidth, window.innerHeight );

	document.getElementById("canvasContainer").appendChild(renderer.domElement);

	var axisHelper = new THREE.AxisHelper( 10 );

	// -5 , 3 , -10
	axisHelper.position.set(0, 0, 0);
	camera.position.set(ApiService.pos.x, ApiService.pos.y, ApiService.pos.z);

	scene.add(axisHelper);
	scene.add(camera);

	var intervalID = setInterval(function() {
				$http.get(url + '/' + shapeFactory.clientId).then(function(res){
					angular.forEach(res.data, function(cli) {
						var client = shapeFactory.clientsConnected[cli.id];

						if(client) {
								client.shape.rotation.x = cli.pos.x;
								client.shape.rotation.y = cli.pos.y;
								client.shape.rotation.z = cli.pos.z;
						} else {
								var c = {
									id: cli.id,
									pos: cli.pos,
									shape: cli.shape,
									color: cli.color
								};
								var shape = shapeFactory.getShape({
									shape: c.shape,
									color: c.color
								});
								shape.position.set(c.pos.x, c.pos.y, c.pos.z);
								scene.add(shape);
								c.shape = shape;
								shapeFactory.clientsConnected[c.id] = c;
						}
					});

			});
		}, 100);

	function render() {
		requestAnimationFrame(render);
		renderer.render(scene, camera);
	//	effect.render(scene,camera);
	}

	render();

	$ionicPlatform.ready(function() {
		var pos = { x: 0, y: 0, z: 0 };

		 window.addEventListener("deviceorientation", function(event) {
				// document.getElementById("x3").innerHTML="("+event.alpha+","+event.beta+","+event.gamma+")"
				camera.rotation.y = -Math.round(event.alpha) * Math.PI / 180;
				camera.rotation.x = Math.round(event.beta) * Math.PI / 180;

				pos.x = Math.round(event.beta) * Math.PI / 180;
				pos.y = Math.round(event.alpha) * Math.PI / 180;
				pos.z = Math.round(event.gamma) * Math.PI / 180;

		 });

		var intervalID2 = setInterval(function() {
			$http({
        method: 'POST',
        url: url,
        data: {
          id: shapeFactory.clientId,
          pos: pos
        },
        headers: {
          'Content-Type': 'application/json '
        }
      });

		}, 100);

	});

});
